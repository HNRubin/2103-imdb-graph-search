import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import java.io.IOException;
import java.util.List;

public class GraphTester {

    private IMDBGraph graph;
    private GraphSearchEngine searchEngine;

    @Before
    public void setup() throws IOException {
        graph = new IMDBGraphImpl("actors_test.list", "actresses_test.list");
        searchEngine = new GraphSearchEngineImpl();
    }

    @Test
    public void testParsing() {
        ActorNode node = (ActorNode) graph.getActor("Actor1");
        assertNotNull(node);
        assertEquals(node.getName(), "Actor1");
        assertEquals(node.getNeighbors().size(), 1);

        ActorNode node2 = (ActorNode) graph.getActor("Actor2");
        assertNotNull(node2);
        assertEquals(node2.getName(), "Actor2");
        assertEquals(node2.getNeighbors().size(), 1);

        ActorNode node3 = (ActorNode) graph.getActor("Actor3");
        assertNotNull(node3);
        assertEquals(node3.getName(), "Actor3");
        assertEquals(node3.getNeighbors().size(), 1);
    }

    @Test
    public void noPath() {
        assertNull(searchEngine.findShortestPath(graph.getActor("Actor1"), graph.getActor("Actor2")));
    }

    @Test
    public void shortestPath() {
        List<Node> shortedPath = searchEngine.findShortestPath(graph.getActor("Actress1"), graph.getActor("Actress5"));
        assertNotNull(shortedPath);
        assertEquals(shortedPath.size(), 7);
    }

}
