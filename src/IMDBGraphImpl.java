import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IMDBGraphImpl implements IMDBGraph {

    private final Map<String, ActorNode> actors = new HashMap<>();
    private final Map<String, MovieNode> movies = new HashMap<>();


    // Implement me
    public IMDBGraphImpl(String actorsFilename, String actressesFilename) throws IOException {
        processActors(actorsFilename);
        processActors(actressesFilename);
        actors.entrySet().removeIf(e -> e.getValue().getNeighbors().size() == 0);
        System.out.println(actors.size());
    }


    // Implement me
    public Collection<? extends Node> getActors() {
        return this.actors.values();
    }

    // Implement me
    public Collection<? extends Node> getMovies() {
        return this.movies.values();
    }

    // Implement me
    public Node getMovie(String name) {
        return this.movies.get(name);
    }

    // Implement me
    public Node getActor(String name) {
        return this.actors.get(name);
    }

    /**
     * Parses the movie title from a line containing a movie
     *
     * @param str line containing a movie
     * @return the movie title
     */
    protected static String parseMovieName(String str) {
        int idx1 = str.indexOf("(");
        int idx2 = str.indexOf(")", idx1 + 1);
        return str.substring(0, idx2 + 1);
    }

    /**
     * Scans an IMDB file for its actors/actresses and movies
     *
     * @param filename the movie file to parse
     */
    protected void processActors(String filename) throws IOException {
        final Scanner s = new Scanner(new File(filename), "ISO-8859-1");

        // Skip until:  Name...Titles
        while (s.hasNextLine()) {
            String line = s.nextLine();
            if (line.startsWith("Name") && line.indexOf("Titles") >= 0) {
                break;
            }
        }

        s.nextLine();  // read one more

        ActorNode actor = null;
        while (s.hasNextLine()) {
            final String line = s.nextLine();

            //System.out.println(line);
            if (line.indexOf("\t") >= 0) {  // new movie, either for an existing or a new actor
                int idxOfTab = line.indexOf("\t");
                if (idxOfTab > 0) {  // not at beginning of line => new actor
                    String actorName = line.substring(0, idxOfTab);

                    ActorNode current;
                    if(!actors.containsKey(actorName)) {
                        current = new ActorNode(actorName);
                        actors.put(actorName, current);
                    }else{
                        current = actors.get(actorName);
                    }
                    actor = current;

                }
                if (/*line.indexOf("(VG)") < 0 &&*/ line.indexOf("(TV)") < 0 && line.indexOf("\"") < 0) {  // Only include bona-fide movies
                    int lastIdxOfTab = line.lastIndexOf("\t");
                    final String movieName = parseMovieName(line.substring(lastIdxOfTab + 1));

                    MovieNode movieNode = movies.get(movieName);
                    if (movieNode == null) {
                        movieNode = new MovieNode(movieName);
                        movies.put(movieName, movieNode);
                    }

                    actor.addMovie(movieNode);
                    movieNode.addActor(actor);
                }

            }
        }
    }
}
