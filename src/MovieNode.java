import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class MovieNode implements Node {

    public MovieNode(String name) {
        this.name = name;
    }

    private String name;
    private Set<ActorNode> actors = new HashSet<>();

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Collection<? extends Node> getNeighbors() {
        return this.actors;
    }

    public void addActor(ActorNode node) {
        this.actors.add(node);
    }

}
