import java.util.*;

public class GraphSearchEngineImpl implements GraphSearchEngine {


    @Override
    public List<Node> findShortestPath(Node s, Node t) {

        //Store a map of how far each node reached is away from the starting node.
        Map<Node, Integer> distanceMap = new HashMap<>();
        Queue<Node> queue = new LinkedList<>();

        //Start the queue.
        queue.add(s);
        distanceMap.put(s, 0);

        boolean found = false;
        while (!queue.isEmpty()) {
            Node currentNode = queue.poll();
            int distance = distanceMap.get(currentNode);
            if (currentNode.equals(t)) {
                found = true;
                break;
            } else {
                //Add each neighbor to the queue.
                for (Node neighbor : currentNode.getNeighbors()) {
                    if (!distanceMap.containsKey(neighbor)) {
                        queue.offer(neighbor);
                        distanceMap.put(neighbor, distance + 1);
                    }
                }
            }
        }

        //If there's no path, return null.
        if (!found)
            return null;


        //Backtrack to find the shorted path.
        //Cannot be generalized to List<Node> because we used addFirst
        LinkedList<Node> shortestPath = new LinkedList<>();
        shortestPath.addFirst(t);
        Node n = t;
        while (n != s) {
            for (Node neighbor : n.getNeighbors()) {
                Integer currentDistance = distanceMap.get(n);
                if (currentDistance == null)
                    continue;
                Integer neighborDistance = distanceMap.get(neighbor);
                if (neighborDistance == null)
                    continue;
                if (currentDistance - 1 == neighborDistance) {
                    shortestPath.addFirst(neighbor);
                    n = neighbor;
                    break;
                }
            }
        }

        return shortestPath;
    }

}
