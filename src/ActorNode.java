import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ActorNode implements Node{

    public ActorNode(String name) {
        this.name = name;
    }

    private String name;
    private Set<MovieNode> movies = new HashSet<>();

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Collection<? extends Node> getNeighbors() {
        return this.movies;
    }

    public void addMovie(MovieNode node) {
        this.movies.add(node);
    }

}
